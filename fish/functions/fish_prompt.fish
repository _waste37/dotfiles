function fish_prompt
    printf '%s@%s %s%s %% ' (set_color -o $fish_color_user)$USER (set_color normal)$hostname (prompt_pwd) (fish_git_prompt)
end
