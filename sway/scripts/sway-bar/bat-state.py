#!/usr/bin/env python3
import psutil

print("bat: ", psutil.sensors_battery().percent, "%", end='', sep='')
