#!/usr/bin/env python3
import psutil

print(psutil.cpu_percent(interval=0.2), "% ", 
      psutil.sensors_temperatures()['coretemp'][0][1], "°C", end='', sep='')
