#!/usr/bin/env python3

import psutil

print("mem: ", psutil.virtual_memory()[2], "%", sep='', end='')
