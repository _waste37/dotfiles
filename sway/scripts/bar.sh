# #!/bin/bash
green=#a7c080
red=e67e80
fg0=#d8caac
fg1=#828f97
bg0=#21272cee
bg1=#3c474d
bg2=#505a60

separator_angle() 
{
  echo -n "{"
  echo -n "\"full_text\":\"\"," # CTRL+Ue0b2
  echo -n "\"separator\":false,"
  echo -n "\"separator_block_width\":0,"
  echo -n "\"border\":\"$bg_bar_color\","
  echo -n "\"border_left\":0,"
  echo -n "\"border_right\":0,"
  echo -n "\"border_top\":2,"
  echo -n "\"border_bottom\":2,"
  echo -n "\"color\":\"$1\","
  echo -n "\"background\":\"$2\""
  echo -n "}"
}

separator_fill() 
{
  echo -n "{"
  echo -n "\"full_text\":\"\"," # CTRL+Ue0b2
  echo -n "\"separator\":false,"
  echo -n "\"separator_block_width\":0,"
  echo -n "\"border\":\"$bg_bar_color\","
  echo -n "\"border_left\":0,"
  echo -n "\"border_right\":0,"
  echo -n "\"border_top\":2,"
  echo -n "\"border_bottom\":2,"
  echo -n "\"color\":\"$1\","
  echo -n "\"background\":\"$2\""
  echo -n "}"
}

common ()
{
    echo -n "\"separator\":false,"
    echo -n "\"separator_block_width\":0",
    echo -n "\"border_left\":0,"
    echo -n "\"border_right\":0,"
    echo -n "\"border_top\":2,"
    echo -n "\"border_bottom\":2,"
}

net_info () 
{
    local text_color=$green
    local net_interface=$(nmcli -t -f DEVICE,NAME connection show --active | sed 's/:Wired connection.*$// ; s/:/::/' | tr "\n" " ")
    if [ -z $net_interface ]; then 
        net_interface=down
        text_color=$red
    fi
    separator_fill $text_color $bg0
    echo -n ",{"
    echo -n "\"name\":\"network\","
    echo -n "\"full_text\":\"  ${net_interface} \","
    echo -n "\"color\":\"$bg1\","
    echo -n "\"background\":\"$text_color\","
    common
    echo -n "},"
    separator_fill $bg2 $text_color
}



cpu_statistics () 
{
    echo -n ",{"
    echo -n "\"name\":\"cpu\","
    echo -n "\"full_text\":\"  $(/home/waste/.config/sway/scripts/sway-bar/cpu-stats.py) \","
    echo -n "\"color\":\"$fg0\","
    echo -n "\"background\":\"$bg2\","
    common
    echo -n "},"
    separator_angle $fg1 $bg2
}

mem_free () 
{
    echo -n ",{"
    echo -n "\"name\":\"mem\","
    echo -n "\"full_text\":\" $(/home/waste/.config/sway/scripts/sway-bar/free-memory.py) \","
    echo -n "\"color\":\"$fg0\","
    echo -n "\"background\":\"$bg2\","
    common
    echo -n "},"
    separator_fill $bg1 $bg2
}

volume()
{
    local volume=$(pamixer --get-volume-human)
    local color=$fg0
    if [ "$volume" = "muted" ]; then
        color=$fg1
    fi
    echo -n ",{"
    echo -n "\"name\":\"volume\","
    echo -n "\"full_text\":\" vol ${volume} \","
    echo -n "\"color\":\"$color\","
    echo -n "\"background\":\"$bg1\","
    common
    echo -n "},"
    separator_angle $fg1 $bg1
}

battery_state()
{

    local battery_charge=$(cat /sys/class/power_supply/BAT0/capacity)

    echo -n ",{"
    echo -n "\"name\":\"battery\","
    echo -n "\"full_text\":\"  ${battery_charge}% \","
    echo -n "\"color\":\"$fg0\","
    echo -n "\"background\":\"$bg1\","
    common
    echo -n "},"
    separator_fill $bg0 $bg1
}

clock()
{
    echo -n ",{"
    echo -n "\"name\":\"time\","
    echo -n "\"full_text\":\" $(date "+%d-%m-%y %H:%M") \","
    echo -n "\"color\":\"$fg0\","
    echo -n "\"background\":\"$bg0\","
    common
    echo -n "},"
    separator_angle $bg2 $bg0
    echo -n ","
}


statusbar()
{
	echo ",["
    net_info
    cpu_statistics
    mem_free
    volume
    battery_state
    clock
    echo "]"
}

echo '{"version": 1, "click_events":true }'

# Begin the endless array.
echo '['

# We send an empty first array of blocks to make the loop simpler:
echo '[]'

# Now send blocks with information forever:
(while :; do
    #retrieve information
    #mem_usage=$(free -m|grep Mem:|tr -s " "|cut -d ' ' -f 3)
    #send JSON to sway
    statusbar
	sleep 1
done) &

# click events

while read line;
do
    case "$line" in
        *"volume"*"button"*" 4,"*) pactl set-sink-volume @DEFAULT_SINK@ +5% ;;
        *"volume"*"button"*" 5,"*) pactl set-sink-volume @DEFAULT_SINK@ -5% ;;
        *"cpu"*"button"*" 1,"*) footclient -e htop ;;
        *"mem"*"button"*" 1,"*) footclient -e htop ;;
        *) ;;
    esac
done
