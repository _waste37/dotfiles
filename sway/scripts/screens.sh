#!/bin/bash

if [ $1 == "-e" ] && [ "$(swaymsg -t get_outputs | grep -o 'HDMI-A-1')" != "" ]
then
    swaymsg output LVDS-1 disable
    swaymsg output HDMI-A-1 enable
elif [ $1 == "-i" ]
then
    swaymsg output LVDS-1 enable
    swaymsg output HDMI-A-1 disable
elif [ $1 == "-b" ]
then
    swaymsg output LVDS-1 enable
    swaymsg output HDMI-A-1 enable
else
    if [ "$(swaymsg -t get_outputs | grep -o 'HDMI-A-1')" != "" ]; then
        swaymsg output LVDS-1 disable
        swaymsg output HDMI-A-1 enable
    fi
fi
