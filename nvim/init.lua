-------------------- ALIASES -------------------------------
local cmd = vim.cmd  -- to execute Vim commands e.g. cmd('pwd')
local fn = vim.fn    -- to call Vim functions e.g. fn.bufnr()
local g = vim.g      -- a table to access global variables
local opt = vim.opt  -- to set options

-------------------- OPTIONS --------------------
opt.expandtab = true     -- Tab are spaces
opt.tabstop = 4          -- 4 spaces in particular
opt.shiftwidth = 4
opt.termguicolors = true -- True color support
opt.number = true
opt.relativenumber = true
vim.g.everforest_transparent_background = true
-- cursor shape
opt.guicursor = { 
    n = 'block',
    v = 'block',
    i = 'block',
}

cmd('colorscheme everforest')

require('lualine').setup{
    options = {
        theme = 'everforest'
    }
}

return require('packer').startup(function()
    use 'wbthomason/packer.nvim'
    use { 
	    'nvim-lualine/lualine.nvim',
        requires = { 'kyazdani42/nvim-web-devicons', opt = true }
    }
    use 'sainnhe/everforest'
end)
